import { xmlFileToJson } from "./helper/FileReader";
import { jsonToRedisFormat } from "./helper/Mapper";
import { createClient } from "redis";

import { argv } from "process";

const filePath = argv[2];
const shouldPrintResult = argv[3];
if (!filePath) {
  throw new Error("No file path provided");
}
const redisClient = createClient();

redisClient.on("error", function (error) {
  console.error(`❗️ Redis Error: ${error}`);
});

redisClient.on("connect", () => {
  console.log("✅ 💃 connect redis success !");

  const data = xmlFileToJson(filePath);
  const result = jsonToRedisFormat(data, redisClient);

  if (shouldPrintResult === "v") {
    console.log(result);
  }
});

redisClient.connect();
