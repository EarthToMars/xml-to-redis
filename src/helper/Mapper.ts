export const jsonToRedisFormat = (data: any, redisClient: any) => {
  let toReturn = {
    subdomains: [],
  };
  if (data?.config?.subdomains?.subdomain?.length > 0) {
    toReturn.subdomains = data.config.subdomains.subdomain;
    redisClient.set(
      "subdomains",
      JSON.stringify(data.config.subdomains.subdomain)
    );
  }
  if (data?.config?.cookies?.cookie?.length > 0) {
    data.config.cookies.cookie.forEach((cookie) => {
      toReturn[`cookie:${cookie.name}:${cookie.host}`] = cookie.$t;
      redisClient.set(`cookie:${cookie.name}:${cookie.host}`, cookie.$t);
    });
  }
  return toReturn;
};
