import fs from "fs";
import parser from "xml2json";

export const xmlFileToJson = (filePath: string) => {
  try {
    const data = fs.readFileSync(filePath);
    var json = JSON.parse(parser.toJson(data));
    return json;
  } catch (err) {
    throw new Error(JSON.stringify(err));
  }
};
